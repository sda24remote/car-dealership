package enums;

public enum EmployeeType {
    MECHANIC, ACCOUNTANT, SALESPERSON, MANAGER
}
