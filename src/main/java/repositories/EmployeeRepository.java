package repositories;

import config.SessionTransaction;
import entities.Accountant;
import entities.Employee;
import entities.Mechanic;
import org.hibernate.query.Query;

import java.util.List;

public class EmployeeRepository extends SessionTransaction {
    public void createEmployee(Employee employee) {
        openSession();
        session.save(employee);

        System.out.println("Employee saved successfully: \n" + employee);
        closeSession();
    }

    public void deleteEmployee(Employee employee) {
        openSession();
        session.delete(employee);
        closeSession();
    }

    public void updateEmployee(Employee employee) {
        openSession();
        session.saveOrUpdate(employee);
        closeSession();
    }

    public List<Employee> findEmployeeById(String id) {
        openSession();

        Query query = session.createQuery("FROM Employee WHERE id = '" + id + "'");
        List<Employee> employees = query.list();

        closeSession();
        System.out.println(employees);
        return employees;
    }

    public List<Accountant> findAllAccountants() {
        openSession();

        Query query = session.createQuery("FROM Employee WHERE type = 'accountant'");
        List<Accountant> accountants = query.list();

        closeSession();
        System.out.println(accountants);
        return accountants;
    }

    public List<Mechanic> findAllMechanics() {
        openSession();

        Query query = session.createQuery("FROM Employee WHERE type = 'mechanic'");
        List<Mechanic> mechanics = query.list();

        closeSession();
        System.out.println(mechanics);
        return mechanics;
    }
}
