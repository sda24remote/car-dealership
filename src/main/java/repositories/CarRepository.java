package repositories;

import config.SessionTransaction;
import entities.Car;
import org.hibernate.query.Query;

import java.util.List;

public class CarRepository extends SessionTransaction {
    public void createCar(Car car) {
        openSession();
        session.save(car);

        System.out.println("Car saved successfully: \n" + car);
        closeSession();
    }

    public void deleteCar(Car car) {
        openSession();
        session.delete(car);
        closeSession();
    }

    public void updateCar(Car car) {
        openSession();
        session.saveOrUpdate(car);
        closeSession();
    }

    public List<Car> findCarById(String id) {
        openSession();

        Query query = session.createQuery("FROM Car WHERE id = '" + id + "'");
        List<Car> cars = query.list();

        closeSession();
        System.out.println(cars);
        return cars;
    }
}
