package repositories;

import config.SessionTransaction;
import entities.Part;
import org.hibernate.query.Query;

import java.util.List;

public class PartRepository extends SessionTransaction {
    public void createPart(Part part) {
        openSession();
        session.save(part);

        System.out.println("Part saved successfully: \n" + part);
        closeSession();
    }

    public void deletePart(Part part) {
        openSession();
        session.delete(part);
        closeSession();
    }

    public void updatePart(Part part) {
        openSession();
        session.saveOrUpdate(part);
        closeSession();
    }

    public List<Part> findPartById(String id) {
        openSession();

        Query query = session.createQuery("FROM Part WHERE id = '" + id + "'");
        List<Part> parts = query.list();

        closeSession();
        System.out.println(parts);
        return parts;
    }
}
