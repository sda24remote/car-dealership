package repositories;

import config.SessionTransaction;
import entities.Client;
import org.hibernate.query.Query;

import java.util.List;

public class ClientRepository extends SessionTransaction {
    public void createClient(Client client) {
        openSession();
        session.save(client);

        System.out.println("Client saved successfully: \n" + client);
        closeSession();
    }

    public void deleteClient(Client client) {
        openSession();
        session.delete(client);
        closeSession();
    }

    public void updateClient(Client client) {
        openSession();
        session.saveOrUpdate(client);
        closeSession();
    }

    public List<Client> findClientById(String id) {
        openSession();

        Query query = session.createQuery("FROM Client WHERE id = '" + id + "'");
        List<Client> clients = query.list();

        closeSession();
        System.out.println(clients);
        return clients;
    }
}
