package config;

import entities.*;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.EntityManager;
import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static Properties properties = new Properties();

    public static SessionFactory getSessionFactory() {
        properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        properties.put(Environment.URL, "jdbc:mysql://localhost:3306/carDealership");
        properties.put(Environment.USER, "flightsuser");
        properties.put(Environment.PASS, "flightspwd");

        properties.put(Environment.HBM2DDL_AUTO, "update");

        properties.put(Environment.SHOW_SQL, true);
        properties.put(Environment.FORMAT_SQL, true);
        properties.put(Environment.USE_SQL_COMMENTS, true);

        properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        properties.put(Environment.POOL_SIZE, 2);

        try {
            Configuration configuration = new Configuration();

            configuration.setProperties(properties);
            configuration.addAnnotatedClass(Client.class);
            configuration.addAnnotatedClass(Car.class);
            configuration.addAnnotatedClass(Part.class);
            configuration.addAnnotatedClass(Service.class);
            configuration.addAnnotatedClass(Employee.class);
            configuration.addAnnotatedClass(Accountant.class);
            configuration.addAnnotatedClass(Mechanic.class);
            configuration.addAnnotatedClass(ServiceReport.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }
        return sessionFactory;
    }
}
