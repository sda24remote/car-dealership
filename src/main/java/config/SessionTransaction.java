package config;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class SessionTransaction {
    protected Session session;
    private Transaction transaction;

    public  void openSession() {
        session =  HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
    }

    public  void closeSession() {
        transaction.commit();
        session.close();
    }
}
