import entities.*;
import enums.CarBrand;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import repositories.CarRepository;
import repositories.ClientRepository;
import repositories.EmployeeRepository;
import repositories.PartRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    /*private static final SessionFactory sessionFactory;
    private static final EntityManager entityManager;

    static {
        try {
            Configuration configuration = new Configuration()
                    .configure("hibernate.cfg.xml");

            sessionFactory = configuration.buildSessionFactory();
            entityManager = sessionFactory.createEntityManager();

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }*/

    public static void main(String[] args) {
        ClientRepository clientRepository = new ClientRepository();
        CarRepository carRepository = new CarRepository();
        PartRepository partRepository = new PartRepository();
        EmployeeRepository employeeRepository = new EmployeeRepository();

        for (int i = 0; i < 3; i++) {
            Client client = new Client("Teo" + (i+1), "LastName");
            clientRepository.createClient(client);
            carRepository.createCar(createCarFor(client));

            //TODO enum for part types, generate random
            //Part part = new Part(getRandomCarBrandAndModel(getRandomInteger(1, 4)), "engine");
            //partRepository.createPart(part);
        }

        Employee accountant1 = new Accountant("John", "Smith", 321);
        Employee mechanic1 = new Mechanic("Jane", "Doe", CarBrand.FORD);
        employeeRepository.createEmployee(accountant1);
        employeeRepository.createEmployee(mechanic1);

        employeeRepository.findAllAccountants();
        employeeRepository.findAllMechanics();
    }

    private static Car createCarFor(Client client) {
        int randomInteger = getRandomInteger(1, 4);
        Pair<CarBrand, List<String>> randomCarBrandAndModel = getRandomCarBrandAndModel(randomInteger);

        randomInteger = getRandomInteger(1, 2);
        int randomYear = getRandomInteger(1, 2021);
        return new Car(randomCarBrandAndModel.getLeft(),
                randomCarBrandAndModel.getRight().get(randomInteger),
                randomYear,
                client);
    }

    public static int getRandomInteger(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static Pair<CarBrand, List<String>> getRandomCarBrandAndModel(int n) {
        switch (n) {
            case 1:
                return new ImmutablePair<>(CarBrand.VOLKSWAGEN, Arrays.asList("Golf", "Polo"));
            case 2:
                return new ImmutablePair<>(CarBrand.AUDI, Arrays.asList("A4", "A5"));
            case 3:
                return new ImmutablePair<>(CarBrand.BMW, Arrays.asList("3 Series", "5 Series"));
            default: return new ImmutablePair<>(CarBrand.FORD, Arrays.asList("Focus", "Mondeo"));
        }
    }
}
