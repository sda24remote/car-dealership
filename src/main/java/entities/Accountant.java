package entities;

import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("accountant")
@NoArgsConstructor
public class Accountant extends Employee{

    private int serialNumber;

    @OneToMany(mappedBy = "accountant")
    private List<ServiceReport> serviceReports = new ArrayList<>();

    public Accountant(String firstName, String lastName, int serialNumber) {
        super(firstName, lastName);
        this.serialNumber = serialNumber;
        this.type = "accountant";
    }
}
