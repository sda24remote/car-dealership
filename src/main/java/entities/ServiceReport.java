package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@Data
public class ServiceReport {

    @Id
    private int id;

    private Date date;

    @OneToOne
    private Service service;

    @ManyToOne
    @JoinColumn(name = "accountantId", referencedColumnName = "id")
    private Accountant accountant;
}
