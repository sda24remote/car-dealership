package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@NoArgsConstructor
@Data

public class Service {
    @Id
    private int id;

    @ManyToOne
    @JoinColumn(name = "carId", referencedColumnName = "id")
    private Car car;

    @OneToMany(mappedBy = "service")
    private List<Part> parts = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "mechanicId", referencedColumnName = "id")
    private Employee mechanic;

    private int installationPrice;
    private Date dateIn;
    private Date dateOut;

}
