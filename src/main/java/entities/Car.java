package entities;

import enums.CarBrand;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data

public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "clientId", referencedColumnName = "id" )
    private Client client;

    @OneToMany(mappedBy = "car")
    private List<Service> services = new ArrayList<>();

    private CarBrand brand;
    private String model;
    private int yearOfProduction;

    public Car(CarBrand brand, String model, int yearOfProduction, Client client) {
        this.client = client;
        this.brand = brand;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
    }
}
