package entities;

import enums.CarBrand;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("mechanic")
@NoArgsConstructor
public class Mechanic extends Employee{

    private CarBrand specialization;

    @OneToMany(mappedBy = "mechanic")
    private List<Service> services = new ArrayList<>();

    public Mechanic(String firstName, String lastName, CarBrand specialization) {
        super(firstName, lastName);
        this.specialization = specialization;
        this.type = "mechanic";
    }
}
