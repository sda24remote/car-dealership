package entities;

import enums.CarBrand;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data

public class Part {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne @JoinColumn(name = "serviceId", referencedColumnName = "id")
    private Service service;

    private CarBrand brand;
    private String type;

    public Part(CarBrand brand, String type) {
        this.brand = brand;
        this.type = type;
    }
}
